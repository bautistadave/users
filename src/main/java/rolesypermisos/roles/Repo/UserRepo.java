package rolesypermisos.roles.Repo;

import org.springframework.data.jpa.repository.JpaRepository;
import rolesypermisos.roles.domain.User;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUserName(String username);
}
