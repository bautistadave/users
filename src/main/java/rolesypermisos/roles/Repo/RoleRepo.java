package rolesypermisos.roles.Repo;

import org.springframework.data.jpa.repository.JpaRepository;
import rolesypermisos.roles.domain.Role;

public interface RoleRepo extends JpaRepository<Role, Long> {
     Role findByName(String name);
}
