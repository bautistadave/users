package rolesypermisos.roles.Service;

import rolesypermisos.roles.domain.Role;
import rolesypermisos.roles.domain.User;

import java.util.List;

public interface UserService {
    User saveUser(User user);
    Role saveRole(Role role);
    void addRoleToUer(String username, String roleName);
    User getUser(String username);
    List<User> getUsers();
}

